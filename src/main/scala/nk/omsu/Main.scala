package nk.omsu

import java.io._
import java.util
import java.util.BitSet

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
/**
  * Created by nk16 on 06.09.16.
  */

object Main {

  def readBytes(file:File):Array[Byte] = {
    val is = new FileInputStream(file)
    val bytesInp = new Array[Byte](file.length.toInt)
    is.read(bytesInp)
    is.close()
    bytesInp
  }
  def printStr(fileName: String, array: Array[Byte]): Unit ={
    val out = new FileOutputStream(new File(fileName))
    out.write(array)
    out.close()
  }
  // val masI:Array[Int] = Array(0, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
  var masB:Array[Int] = null
  var masT:Array[Int] = null

  val tab:Array[Array[Int]] = Array(
    Array(13, 2, 8, 4, 6,15,11, 1,10, 9, 3,14, 5, 0,12, 7),
    Array(1, 15,13, 8,10, 3, 7, 4,12, 5, 6,11, 0,14, 9, 2),
    Array(7, 11, 4, 1, 9,12,14, 2, 0, 6,10,13,15, 3, 5, 8),
    Array(2,  1,14, 7, 4,10, 8,13,15,12, 9, 0, 3, 5, 6,11)
  )
  var strS:Array[Int] = null

  def byteArrayToBitArray(bytes: Array[Byte]): util.BitSet = {


    val bitsLength = if ((bytes.length * 8) % 6 == 0) bytes.length * 8 else 6 - bytes.length * 8 % 6  + bytes.length * 8
    var bits:util.BitSet = new util.BitSet(bitsLength)
    var bits2:util.BitSet = new util.BitSet(bitsLength)
    bits = util.BitSet.valueOf(bytes)

    strS = new Array[Int](bitsLength/6)
    masB = new Array[Int](bitsLength/6)
    var index = 0
    //println("read:")
    for (i <- 0 until  bitsLength) {
      if (i % 8 == 0){
        if (bits.get(0+i)) bits2.set(i+7)
        if (bits.get(1+i)) bits2.set(i+6)
        if (bits.get(2+i)) bits2.set(i+5)
        if (bits.get(3+i)) bits2.set(i+4)
        if (bits.get(4+i)) bits2.set(i+3)
        if (bits.get(5+i)) bits2.set(i+2)
        if (bits.get(6+i)) bits2.set(i+1)
        if (bits.get(7+i)) bits2.set(i+0)
      }
    }
    bits2 = bits/////////////////////////////////////////////////////////////////////****!!!!
   /* for (i <- 0 until  bitsLength) {
      if (i % 6 == 0)
        print(" ")
      if (bits2.get(i))
        print(1)
      else
        print(0)
    }
*/
   // println()
    //print("Строки: ")
    for (i <- 0 until  bitsLength) {
      if (i % 6 == 0){
        if(!bits2.get(a+i) && !bits2.get(b+i))     strS(index) = 0
        if(!bits2.get(a+i) && bits2.get(b+i))      strS(index) = 1
        if(bits2.get(a+i) && !bits2.get(b+i))      strS(index) = 2
        if(bits2.get(a+i) && bits2.get(b+i))       strS(index) = 3
    //    print(strS(index))
      //  print(' ')
        index+=1
      }
    }

    index = 0
    val stb:Array[String] = new Array[String](bitsLength/6)
 //   println()
   // print("Столбцы: ")
    for (i <- 0 until  bitsLength) {
      if (i % 6 == 0){
        stb(index) = ""
        if(bits2.get(0+i))
          stb(index) += '1'
        else
          stb(index) += '0'
        if(bits2.get(2+i))
          stb(index) += '1'
        else
          stb(index) += '0'
        if(bits2.get(4+i))
          stb(index) += '1'
        else
          stb(index) += '0'
        if(bits2.get(5+i))
          stb(index) += '1'
        else
          stb(index) += '0'
        masB(index) = Integer.parseInt(stb(index), 2)
     //   print(masB(index))
       // print(' ')
        index+=1
      }
    }

    bits2
  }
  def encryption(fileNameE:String, a:Int, b:Int):Unit = {

    val bytesInp = readBytes(new File(fileNameE))
    val bitsInp = byteArrayToBitArray(bytesInp)
    val bitsOutLength = if ((bytesInp.length * 8) % 6 == 0) bytesInp.length * 8 else 6 - bytesInp.length * 8 % 6  + bytesInp.length * 8
    //val bitsOut = new util.BitSet(bitsOutLength)
    val intOut:Array[Byte] = new Array[Byte](bitsOutLength/6)
    for (i <- 0 until bitsOutLength/6){
      intOut(i) = tab(strS(i))(masB(i)).toByte
    }
/*    println()
    for(i <- intOut){
      println(i)
    }
    println()*/
    val bitsTab = util.BitSet.valueOf(intOut)
 /*   for (i <- 0 until  intOut.length*8) {
      if (i % 8 == 0)
        print(" ")
      if (bitsTab.get(i))
        print(1)
      else
        print(0)
    }
    println()*/
    val size = strS.length*6
    var bitsOut = new util.BitSet(size)
    var index = 0
    var sixMod = 0
    for (i <- 0 until strS.length*8){
      if (i % 8 == 0){
        if (strS(index) == 0){
          //bitsOut.set(0+i)
          //bitsOut.set(1+i)
          if (bitsTab.get(0+i)) bitsOut.set(2+sixMod)
          if (bitsTab.get(1+i)) bitsOut.set(3+sixMod)
          if (bitsTab.get(2+i)) bitsOut.set(4+sixMod)
          if (bitsTab.get(3+i)) bitsOut.set(5+sixMod)
        }
        if (strS(index) == 1){
          //bitsOut.set(0+i)
          bitsOut.set(1+sixMod)
          if (bitsTab.get(0+i)) bitsOut.set(2+sixMod)
          if (bitsTab.get(1+i)) bitsOut.set(3+sixMod)
          if (bitsTab.get(2+i)) bitsOut.set(4+sixMod)
          if (bitsTab.get(3+i)) bitsOut.set(5+sixMod)
        }
        if (strS(index) == 2){
          bitsOut.set(0+sixMod)
          //bitsOut.set(1+i)
          if (bitsTab.get(0+i)) bitsOut.set(2+sixMod)
          if (bitsTab.get(1+i)) bitsOut.set(3+sixMod)
          if (bitsTab.get(2+i)) bitsOut.set(4+sixMod)
          if (bitsTab.get(3+i)) bitsOut.set(5+sixMod)
        }
        if (strS(index) == 3){
          bitsOut.set(0+sixMod)
          bitsOut.set(1+sixMod)
          if (bitsTab.get(0+i)) bitsOut.set(2+sixMod)
          if (bitsTab.get(1+i)) bitsOut.set(3+sixMod)
          if (bitsTab.get(2+i)) bitsOut.set(4+sixMod)
          if (bitsTab.get(3+i)) bitsOut.set(5+sixMod)
        }
        sixMod +=6
        index +=1
      }
    }

    var numberSrt:String = ""
    //print(" ")
    for (i <- 0 until  strS.length*6) {
      //if (i % 6 == 0 && i != 0)
      //  print("   ")
      if (bitsOut.get(i)){
   //     print(1)
        numberSrt +='1'
      }
      else{
        numberSrt +='0'
        //print(0)
      }
    }
    //println()
    printStr(fileNameE, bitsOut.toByteArray)






  }
  def decryption(fileNameD:String, a:Int, b:Int):Unit ={
    val bytesInp = readBytes(new File(fileNameD))
    var bits:util.BitSet = new util.BitSet()
    var bitsOut:util.BitSet = new util.BitSet()
    var bits2:util.BitSet = new util.BitSet()

    bits = util.BitSet.valueOf(bytesInp)
    bitsOut = new util.BitSet()

  /*  print(" ")
    for (i <- 0 until  bits.length()) {
      if (i % 6 == 0 && i != 0)
        print("   ")
      if (bits.get(i)){
        print(1)

      }
      else{

        print(0)
      }
    }*/
    for (i <- 0 until  bits.length) {
      if (i % 6 == 0){
        if (bits.get(2+i)) bits2.set(i+5)
        if (bits.get(3+i)) bits2.set(i+4)
        if (bits.get(4+i)) bits2.set(i+3)
        if (bits.get(5+i)) bits2.set(i+2)
        if (bits.get(1+i)) bits2.set(i+1)
        if (bits.get(0+i)) bits2.set(i+0)
      }
    }

    var flag = false
    bits = bits2
    var len = 0
    if (bits.length % 6 != 0){
      flag = true
      len = 6 - bits.length() % 6  + bits.length
      bits.set(6 - bits.length() % 6  + bits.length() - 1)
    }
   /* println()
    for (i <- 0 until  bits.length()) {
      if (i % 6 == 0 && i != 0)
        print("   ")
      if (bits.get(i)){
        print(1)

      }
      else{

        print(0)
      }
    }
    println()
    print("Строки: ")*/
    strS = new Array[Int](bits.length/6)
    masB = new Array[Int](bits.length/6)
    var index = 0
    for (i <- 0 until  bits.length()) {
      if (i % 6 == 0){
        if(!bits.get(0+i) && !bits.get(1+i))     strS(index) = 0
        if(!bits.get(0+i) && bits.get(1+i))      strS(index) = 1
        if(bits.get(0+i) && !bits.get(1+i))      strS(index) = 2
        if(bits.get(0+i) && bits.get(1+i))       strS(index) = 3
       // print(strS(index))
       // print(' ')
        index+=1
      }
    }
    index = 0
    val stb:Array[String] = new Array[String](bits.length()/6)
    //println()
    //print("Пересечение: ")
    for (i <- 0 until  bits.length()) {
      if (i % 6 == 0){
        stb(index) = ""
        if(bits.get(2+i))
          stb(index) += '1'
        else
          stb(index) += '0'
        if(bits.get(3+i))
          stb(index) += '1'
        else
          stb(index) += '0'
        if(bits.get(4+i))
          stb(index) += '1'
        else
          stb(index) += '0'
        if(bits.get(5+i))
          if(flag && (5+i == (len-1)))
            stb(index) += '0'
          else
            stb(index) += '1'
        else
          stb(index) += '0'
        masB(index) = Integer.parseInt(stb(index), 2)
       // print(masB(index))
       // print(' ')
        index+=1
      }
    }
 //   println()
   // print("столбцы: ")
    masT = new Array[Int](bits.length/6)
    for(j <- strS.indices)
      for(i <- tab(0).indices){
        if(tab(strS(j))(i) == masB(j)){
          masT(j) = i
 //         print(" ")
   //       print(masT(j))
        }
      }
println()

    var masTBinary:Array[Byte] = new Array[Byte](masT.length * 6)
    var masBit:Array[String] = new Array[String](masT.length * 6)
    for (i <-  masT.indices) {
      masTBinary(i) =  masT(i).toByte
      masBit(i) = Integer.toBinaryString(masT(i))
      while (masBit(i).length % 6 != 0){

        masBit(i) = "0" + masBit(i)
      }
    //  print(" ")
      //print(masBit(i))
    }
    var strBit:String = ""
    for (s<- masBit){
      if(s != null)
      strBit += s

    }
   // println()
  //  println(strBit)
    var strBit2 = new util.BitSet()
    for (i <- 0 until  masBit.length) {
      if (i % 6 == 0){
        if(bits.get(0+i))     strBit2.set(i+1)
        if(bits.get(1+i))     strBit2.set(i+3)
          if (strBit(2+i) == '1') strBit2.set(i+0)
        if (strBit(3+i) == '1')strBit2.set(i+2)
        if (strBit(4+i) == '1') strBit2.set(i+4)
        if (strBit(5+i) == '1') strBit2.set(i+5)
      }
    }
   /* println()
    for (i <- 0 until  strBit.length()) {
      if (i % 6 == 0 && i != 0)
        print("   ")
      if (strBit2.get(i)){
        print(1)
      }
      else{

        print(0)
      }
    }
    println()*/
    var bytes = strBit2.toByteArray
    printStr(fileNameD, bytes)
    /*  val out = new PrintWriter("/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/lab5(sbt, scala)/src/main/resources/d.txt")
      for(i <- intOut)
        out.print(i)
      out.close()*/
  }
  val a = 2 - 1
  val b = 4 - 1
  def main(args:Array[String]) :Unit ={

    val filenameE = readLine("Input: ")// "/home/nk16/Dropbox/1 семестр/криптографические методы защиты информации/LABS/lab5(sbt, scala)/src/main/resources/f.txt"

    for (i <- tab.indices){
      for (j <-  tab(i)){
        print(j)
        print(' ')
      }
      println()
    }

   // encryption(filenameE, a, b)//readLine("Input: ")
    decryption(filenameE, a ,b)
  }
}